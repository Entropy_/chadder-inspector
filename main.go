package main

import (
  "os"
  "log"
  "fmt"
  "flag"
  "time"
  "bytes"
  "strconv"
  "strings"
  "net/url"
  "os/exec"
  "net/http"
  "image"
  "gocv.io/x/gocv"
  "image/color"
  "image/draw"
  "image/png"
  "io/ioutil"
  "encoding/json"
  "golang.org/x/image/font"
  "github.com/golang/freetype"
  "github.com/kurrik/oauth1a"
  "github.com/kurrik/twittergo"
)
//For drawing
var (
  dpi  = flag.Float64("dpi", 72, "Screen resolution in dots per inch")
  fontfile = flag.String("fontfile", "fonts/techno_hideo.ttf", "techno_hideo.ttf")
  hinting = flag.String("hinting", "none", "none | full")
  size = flag.Float64("size", 36, "font size in points")
  spacing = flag.Float64("spacing", 1.5, "line spacing, 2 means double spaced")
  wonb = flag.Bool("whiteonblack", false, "white text on a black background")
)



func LoadCreds() (client *twittergo.Client, err error) {
  creds, err := ioutil.ReadFile("CREDENTIALS.ch")
  if err != nil {
    panic(err)
  }
  log.Print("\033[38:2:150:150:0mCredentials file loaded.\033[0m")
  lines := strings.Split(string(creds), "\n")

  conf := &oauth1a.ClientConfig{
    ConsumerKey:  lines[0],
    ConsumerSecret:  lines[1],
  }
  usr := oauth1a.NewAuthorizedConfig(lines[2], lines[3])
  client = twittergo.NewClient(conf, usr)
  return client, err
}

type Arguments struct {
  ScreenN string
  OutputF string
}

func parseArguments() *Arguments {
  a := &Arguments{}
  flag.StringVar(&a.ScreenN, "screen_name", "twitterapi", "Screen name")
  flag.StringVar(&a.OutputF, "out", "output.json", "Output file")
  return a
}



func main() {
  if len(os.Args) < 2 || len(os.Args) > 2 {
    log.Print("\033[38:2:255:0:0mWRONG NUMBER OF ARGUMENTS PASSED\033[0m")
    log.Print("\033[38:2:0:255:0mUsage is `./chadder-inspector USERNAME`\033[0m")
    os.Exit(1)
  }
  flag.Parse()
  //Declaration block
  var (
    err  error
    client  *twittergo.Client
    req  *http.Request
    resp  *twittergo.APIResponse
    args  *Arguments
    max_id  uint64
    out  *os.File
    query  url.Values
    results  *twittergo.Timeline
    text  []byte
  )

  //Get the font
  fontBytes, err := ioutil.ReadFile(*fontfile)
  if err != nil {
    panic(err)
  }
  f, err := freetype.ParseFont(fontBytes)
  if err != nil {
    panic(err)
  }

  //Context!
  fg, bg := image.Black, image.White
  ruler := color.RGBA{0xdd, 0xdd, 0xdd, 0xff}

  cmd := exec.Command("./../babelsalmon/babelsalmon", "../babelsalmon/library/loveofthewild.txt")

  var salmonBytes bytes.Buffer
  cmd.Stdout = &salmonBytes
  err = cmd.Run()
  if err != nil {
    fmt.Println(err)
    //panic(err)
  }
  textInput := salmonBytes.String()

  if *wonb {
    fg, bg = image.White, image.Black
    ruler = color.RGBA{0x22, 0x22, 0x22, 0xff}
  }
  rgba := image.NewRGBA(image.Rect(0, 0, 3000, 100))
  draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)
  c := freetype.NewContext()
  c.SetDPI(*dpi)
  c.SetFont(f)
  c.SetFontSize(*size)
  c.SetClip(rgba.Bounds())
  c.SetDst(rgba)
  c.SetSrc(fg)
  switch *hinting {
    default:
    c.SetHinting(font.HintingNone)
  case "full":
    c.SetHinting(font.HintingFull)
  }

  for i := 0;i < 200; i++ {
    rgba.Set(10, 10+i, ruler)
    rgba.Set(10+i, 10, ruler)
  }

  //draw the text
  pt := freetype.Pt(10, 10+int(c.PointToFixed(*size)>>6))
  for s := 0;s < 1;s++ {
    _, err = c.DrawString(string(textInput), pt)
    if err != nil {
      panic(err)
    }
    pt.Y += c.PointToFixed(*size * *spacing)
  }
  window := gocv.NewWindow("MARKOV")
  defer window.Close()


  boofer, err := os.Create("outputs/image.png")
  if err != nil {
    panic(err)
  }
  defer boofer.Close()
  png.Encode(boofer, rgba)
  boofer.Sync()




  var buffer bytes.Buffer
  log.New(&buffer, "chadder", log.Llongfile)
  log.Print("\033[38:2:150:150:0mLet's inspect a chadder!\033[0m")


  //Start consuming the variables
  args = parseArguments()
  client, err = LoadCreds()
  if err != nil {
    panic(err)
  }

  out, err = os.Create(args.OutputF)
  if err != nil {
    panic(err)
  }
  defer out.Close()

  //List the api endpoints needed
  const (
    count int = 200
    urllist  = "/1.1/statuses/user_timeline.json?%v"

    minWait = time.Duration(10) *time.Second
  )

  //Start the query setup
  query = url.Values{}
  query.Set("count", fmt.Sprintf("%v", count))
  query.Set("screen_name", os.Args[1])
  total := 0

  //Create an array to hold the tweet ids to unretweet
  var tweeties []twittergo.Tweet
  var tweetIds []uint64
  var replyArray []string
  var incantation string
  //Start the main loop
  for {
    if max_id != 0 {
      query.Set("max_id", fmt.Sprintf("%v", max_id))
    }

    endpoint := fmt.Sprintf(urllist, query.Encode())
    req, err = http.NewRequest("GET", endpoint, nil)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:50:50:200mGetting a list of statuses.\033[0m")

    resp, err = client.SendRequest(req)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:150:150:0mRequest status "+resp.Status+"\033[0m")

    results = &twittergo.Timeline{}
    err = resp.Parse(results)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:50:50:200mParsing and doing the thing.\033[0m")

    //TODO the thing
    if rle, ok := err.(twittergo.RateLimitError); ok {
      dur := rle.Reset.Sub(time.Now()) + time.Second
      if dur < minWait {
        dur = minWait
      }
      log.Print("\033[38:250:50:50mRate limited, waiting...\033[0m")
      time.Sleep(dur)
      continue
    } else {
      log.Print("\033[38:250:50:50mError parsing reponse.\033[0m")
    }

    batch := len(*results)
    if batch == 0 {
      log.Print("\033[38:150:150:0mNo more results, end of timeline.\033[0m")
      break
    }

    for _, tweet := range *results {

      tweeties = append(tweeties, tweet)
      text, err = json.Marshal(tweet)
      if err != nil {
        panic(err)
      }
      out.Write(text)
      out.Write([]byte("\n"))
      tweetIds = append(tweetIds, tweet.Id())

      if tweet.ReplInt() != "" {
        value := tweet.ReplInt()
        //log.Print("\033[38:2:100:150:100mTweet is in reply to "+fmt.Sprint(value)+"\033[0m")
        replyArray = append(replyArray, value)
      }



      max_id = tweet.Id() - 1
      total += 1
    }
    log.Print("\033[38:2:150:150:0mGot "+strconv.Itoa(batch)+" tweets. Of which "+strconv.Itoa(len(replyArray))+" were replies.\033[0m")
  }


  //Set the screen_name to be tweeted
  var screamie []string
  numberMatched := 0
  for i := range replyArray  {
    replyID, err := strconv.ParseInt(replyArray[i], 10, 64)
    if err != nil {
      panic(err)
    }

    for _, ids := range tweetIds {
      if uint64(replyID) == ids{
        numberMatched++
        incantation += replyArray[i]+","
        if strings.Count(incantation, replyArray[i]) >= 3 {
          timesCursed := fmt.Sprint(strings.Count(incantation, replyArray[i]))

          //loop over the tweets and match the id to the user
          for _, tweet := range tweeties {
            if replyArray[i] == strconv.Itoa(int(tweet.Id())) {
              log.Print("\033[38:2:255:100:50mA hex upon "+tweet.User().ScreenName()+"\033[0m")
              screamie = append(screamie, tweet.User().ScreenName())
            }
          }


          log.Print("\033[38:2:0:150:0m"+timesCursed+" times replied,")
                    log.Print("\033[38:2:150:0:0m"+timesCursed+" times cursed!\033[0m")
        }

      }

    }
  }
  log.Print("\033[38:2:0:150:0m"+strconv.Itoa(numberMatched)+" of these tweets were to this same user\033[0m")





  /*
  for i := range tweetIds {

    log.Print("\033[38:150:150:150mFound "+fmt.Sprint(tweetIds[i])+".\033[0m")
  }*/




}
